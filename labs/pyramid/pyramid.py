import numpy as np
import plotly
import plotly.graph_objs as go
import matplotlib.pyplot as plt

import torch
import torch.nn as nn

import urllib.request
import os.path
import time

from oksm import COUNTRIES


##############################################################################################

def rectify(pop, year):
    ny, na, ns = pop.shape
    res = np.empty((ny+na,na,ns))
    res[:] = np.nan
    y, a = np.meshgrid(np.arange(ny),np.arange(na), indexing='ij') 
    print(res[y, a, :].shape)
    res[y+na-a, a, :] = pop
    
    res_year = np.arange(ny+na)+np.min(year)-na
    return res, res_year

################################################################################################

def median(x, axis=1):
    assert axis==1
    y = np.cumsum(x, axis=1)
    t = y[:,-1,None,:]/2
    a = y>t
    i = np.argmax(a,axis=1)
    p = np.take_along_axis(y,i[:,None,:]-1,axis=1)
    q = np.take_along_axis(y,i[:,None,:],axis=1)
    assert np.all(p<=t) and np.all(t<=q)
    j = (t-p)/(q-p)
    assert np.all(0<=j) and np.all(j<=1)    
    return i+j[:,0,:]

#########################################################################

def plot_pyramid_history(x,year,figsize=(15,7), clim=None,cmap='plasma',origin=None,title=None):
    fig = plt.figure(figsize=figsize)
    ax = fig.add_axes([0,0.15,0.5,0.85])
    bx = fig.add_axes([0.5,0.15,0.5,0.85])
    cx = fig.add_axes([0.25,0,0.5,0.05])    
    ext = (0, x.shape[1], np.min(year), np.max(year))
    if clim is None:
        if origin is None:
            mx = np.nanmax(x)
            mn = np.nanmin(x)    
            clim = (mn, mx)
        else:
            mx = np.nanmax(np.abs(x-origin))
            clim = (origin-mx, origin+mx)

    im=ax.imshow(x[:,:,0],interpolation='none',clim=clim, origin='lower', extent=ext,cmap=cmap,aspect='auto')
    bx.imshow(x[:,:,1],interpolation='none',clim=clim, origin='lower', extent=ext,cmap=cmap,aspect='auto')
    bx.yaxis.tick_right()
    ax.invert_xaxis()
    ax.set_xlabel("Age (male)")
    bx.set_xlabel("Age (female)")
    fig.colorbar(im, cax=cx, orientation='horizontal')
    cx.set_title(title)
    plt.show()

#########################################################################

def plot_pyramid(population, year, pred_population=None, pred_year=None, age=None):
    """
    Функция строит поло-возрастную пирамиду и анимирует ее с помощью Plotly.
    Аргументы:
        population - трехмерный массив с данными о численности населения.
        year - одномерный массив с годами, отвечающими нулевой оси массива population.
        pred_population, pred_year - аналогичны аргументам без pred_, но содержат предсказанные моделью значения. 
    """
    # Находим максимальные и минимальные отклонения по осям.
    mx = np.max(population)
    mn_year, mx_year = np.min(year), np.max(year)
    if pred_population is not None:
        mx=np.maximum(mx, np.max(pred_population))
        mn_year=np.minimum(mn_year, np.min(pred_year))
        mx_year=np.maximum(mx_year, np.max(pred_year))
        
    # Вспомогательная функция, возвращает графики за один год.
    # pop и pop_pred должны быть срезами массивов population и pred_population.
    # Любой из аргументов может быть None.
    def make_data(pop, pop_pred):
        assert pop is None or (isinstance(pop, np.ndarray) and pop.ndim==2)
        assert pop_pred is None or (isinstance(pop_pred, np.ndarray) and pop_pred.ndim==2)
        
        data = []
        if pop_pred is not None:
            data.append(go.Bar(y=age-0.5,
                           x=-pop_pred[:,0],
                           orientation='h',
                           name='Men (pred)',
                           hoverinfo='x',
                           marker=dict(color='cyan')
                           ))
            data.append(go.Bar(y=age-0.5,
                           x=pop_pred[:,1],
                           orientation='h',
                           name='Women (pred)',
                           hoverinfo='x',
                           marker=dict(color='yellow')
                           ))
                
        if pop is not None:
            data.append(go.Bar(y=age,
                           x=-pop[:,0],
                           orientation='h',
                           name='Men',
                           hoverinfo='x',
                           marker=dict(color='blue')
                           ))
            data.append(go.Bar(y=age,
                           x=pop[:,1],
                           orientation='h',
                           name='Women',
                           hoverinfo='x',
                           marker=dict(color='red')
                           ))
        
            
        return data
    
    # Вспомогательная функция, добавляющая подпись на график.
    def make_annotations(y):
        return [
            go.layout.Annotation(
                x=0.98, y=1,
                xref="paper", yref="paper",
                text=f"Year {y}",
                showarrow=False,
            )
        ]
    
    # Описание изображения.
    layout = go.Layout(
        width=700, # Ширина и
        height=700, # высота графика.
        yaxis=go.layout.YAxis(title='Age'), # Подписи по осям.
        xaxis=go.layout.XAxis(title='Number',range=[-mx,mx]),
        barmode='overlay', # Настройки полос.
        bargap=0.5,
        annotations = make_annotations(mn_year), # Подписи.
        updatemenus=[dict( # Кнопки управления анимацией.
            type="buttons",
            buttons=[
                dict(label="Play",method="animate", args=[None]),
                dict(label='Stop',method='animate', args=[[None], {"frame": {"duration": 0, "redraw": False}, "mode": "immediate", "transition": {"duration": 0}}
                ])
            ]
        )],
    )

    # Функция для подготовки кадров анимации.
    yearpop = {y: p for y,p in zip(year, population)}
    pred_yearpop = {y: p for y,p in zip(pred_year, pred_population)} if pred_year is not None else {}
    def make_data_aux(y):
        return make_data(yearpop.get(y),pred_yearpop.get(y))
    
    # Создаем рисунок.
    fig = go.Figure(
        data=make_data_aux(mn_year),
        layout=layout,
        frames=[
            go.Frame(
                data = make_data_aux(k),
                layout = go.Layout( annotations=make_annotations(k) ),
            ) for k in range(mn_year, mx_year+1)
        ]
    )
    
    fig.show() # Выводим рисунок.

    
###########################################################################


class PopulationDataset(torch.utils.data.Dataset):
    """
    Набор данный, состоящий из поло-возрастной пирамиды в начальный момент, пирамиды в конечный момент
    и времени между этими моментами.
    """
    def __init__(self, population, year, maxstep=None):
        """
        Создает набор данных из массива численности населения `population` (см. описание `plot_pyramid`)
        и массива `year` годов сбора данных.  
        """
        assert isinstance(population, np.ndarray) and population.ndim==3
        assert isinstance(year, np.ndarray) and year.ndim==1
        self.population = population
        self.year = year
        self.size = year.shape[0]
        assert population.shape[0]==self.size
        self._prepare(np.max(self.year)-np.min(self.year) if maxstep is None else maxstep)
    def _prepare(self, maxstep):
        self._indices = []
        for k in range(self.size-1):
            for n in range(k+1, self.size):
                step = self.year[n]-self.year[k]
                if step<=maxstep:
                    self._indices.append((k,n,step))
    def __len__(self):
        return len(self._indices)
    def __getitem__(self, k):
        assert isinstance(k, int) and k>=0
        f,s,step = self._indices[k]
        return torch.tensor(self.population[f],dtype=torch.float), torch.tensor(self.population[s],dtype=torch.float), step

class FixedStepDataset(torch.utils.data.Dataset):
    """
    Аналог `PopulationDataset`, но временной шаг только в один год.
    """
    def __init__(self, population, year, step=1):
        assert isinstance(population, np.ndarray) and population.ndim==3
        assert isinstance(year, np.ndarray) and year.ndim==1
        self.population = population
        self.year = year
        self.size = year.shape[0]
        assert population.shape[0]==self.size
        self._prepare(step)        
    def _prepare(self, step):
        self._indices = []
        for k in range(self.size):
            n = k + step
            if n<self.size:
                self._indices.append((k,n,self.year[n]-self.year[k]))
    def __len__(self):
        return len(self._indices)
    def __getitem__(self, k):
        assert isinstance(k, int) and k>=0
        f,s,step = self._indices[k]
        return torch.tensor(self.population[f],dtype=torch.float), torch.tensor(self.population[s],dtype=torch.float), step
    def __repr__(self):
        return f"{self._indices}"

    
##############################################################################

# Коды некоторых стран на сайте.

def get_population_pyramid_one_year(country, year):
    """
    Скачивает поло-возрастную пирамиду с сайта populationpyramid.net.
    Аргументы:
        country - число или название - для какой страны скачивать данные.
        year - за какой год скачивать данные.
    Результат:
        2d массив. Нулевая ось - возрастная группа: 0 = 0-4, 1 = 5-9, 2 = 10-14,...
        Первая ось - пол: 0 - мужской, 1 - женский.
    """
    if isinstance(country, str): country = COUNTRIES[country]
    url = f"https://www.populationpyramid.net/api/pp/{country}/{year}/?csv=true"
    user_agent = 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.7) Gecko/2009021910 Firefox/3.0.7'
    headers={'User-Agent':user_agent,} 
    req = urllib.request.Request(url, headers=headers)
    print('Load', url)
    with urllib.request.urlopen(req) as f:
        data = np.loadtxt(f, skiprows=1, delimiter=',', usecols=(1,2), dtype=int)
    return data

def get_population_pyramid(country, minyear=1950, maxyear=2019, folder='population_pyramid/',pause=5):
    """
    Загружает поло-возрастные пирамида с сайта populationpyramid.net для данной страны за все годы.
    Аргументы:
        country - число или название - для какой страны скачивать данные.
        folder - директория, в которую кешируются данные.
        minyear, maxyear - диапазон лет. 
    Возвращаемые значения:
        population - 3d массив. Нулевая ось - индекс года, сам год можно по индексу прочитать в year,
                    Первая ось и вторая оси совпадают с нулевой и первой осями в результате 
                    `get_population_pyramid_one_year` соответственно.
        year - 1d массив. `population[n]` содержит данные за год `year[n]`.
    """
    if isinstance(country, str): country = COUNTRIES[country]    
    filename = f"{folder}/country{country}.npz"
    if os.path.isfile(filename): # Load cached
        data = np.load(filename)
        return data['population'], data['year']
    population = []
    year = np.arange(minyear, maxyear+1)
    for y in year:
        population.append( get_population_pyramid_one_year(country, y) )
        time.sleep(pause)
    population = np.array(population)
    np.savez(filename, population=population, year=year)
    return population, year  

##############################################################################

def stackbins(x, multiplier):
    last = x[-1:]
    head = x[:-1].view((-1,multiplier)+x.shape[1:]).sum(1)
    return torch.cat((head,last))
    
def unstack(x, weight):
    assert x.ndim==2 and x.shape[0]-1 == weight.shape[0]
    assert weight.ndim == 3 and weight.shape[0]==x.shape[0]-1 and weight.shape[2]==x.shape[1]
    last = x[-1:]
    w = torch.exp(weight)
    w = w/w.sum(1)[:,None]
    head = ( x[:-1,None]*w ).view((-1,)+x.shape[1:])
    return torch.cat((head,last))


##############################################################################
